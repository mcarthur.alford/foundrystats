console.log("Tome of Disappointment | Initialization Started")





//Clamp Function, pretty handy:
Math.clamp = function (number, min, max) {
    return Math.max(min, Math.min(number, max));
}


/**
 * An abstraction of the annoying to type 'diceString'. Basically provides the reference for setflag/getflag given the faces and result.
 * @param  {[type]} foo [description]
 * @return {[type]}     [description]
 */
function diceString(section, faces, result) {
    return `ToD.${section}.${faces}.${result}`;
}


Hooks.on("createChatMessage", (data) => {
    const roll = data._roll;
    if (roll == undefined) return;
    const user = game.user;

    let debugging = false;
    if (game.settings.get("ToD", "DebugMode")) { debugging = true; }



    roll.terms.forEach(element => {
        if (element.constructor.name == "Die") {
            console.log(element);
            const faces = element.faces;
            const results = element.results;
            let holder = {};
            results.forEach(result => {
                let existing = holder[result.result] || parseInt(user.getFlag("world", diceString('flat', faces, result.result))) || 0;
                console.log(diceString('flat', faces, result.result));
                holder[result.result] = existing + 1;
            });
            console.log(holder);
            Object.keys(holder).forEach(key => {
                // console.log(value);
                // console.log(value.constructor);
                console.log(holder[key]);
                user.setFlag("world", diceString('flat', faces, key), `${holder[key]}`);
            });
        };
    });


    loadStatApp();
});


Hooks.once("init", async function () {
    // Registering Settings:
    game.settings.register("ToD", "DebugMode", {
        name: "Tome of Disappointing Debugging",
        hint: "Enable debug mode for tome of disappointment? (Don't touch this unless you know what you are doing).",
        scope: "client",
        type: Boolean,
        default: true,
        config: true
    });
});

console.log("Tome of Disappointment | Initialization Finished")





class StatApp extends FormApplication {
    constructor(exampleOption) {
        super();
        this.exampleOption = exampleOption;
    }

    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ['form'],
            popOut: true,
            template: `modules/Tome of Disappointment/myFormApplication.html`,
            id: 'ToD',
            title: 'Tome of Disappointment',
            width: 600,
            height: 600
        });
    }

    getData() {
        // Return data to the template
        let users = {};
        game.users.forEach(user => {

            let dice = {};
            const diceTypes = [3, 6, 8, 10, 12, 20, 100];
            diceTypes.forEach(type => {
                let faces = {};
                for (let i = 1; i <= type; i++) {
                    const value = parseInt(user.getFlag("world", diceString('flat', type, i))) || 0;
                    faces[`${i}`] = value;
                }
                dice[type] = faces;
            });


            users[user.data.name] = dice;
        });

        return {
            users: users
        };
    }

    activateListeners(html) {
        super.activateListeners(html);
        // TODO make this work.
    //     var ctx = document.getElementById('myChart').getContext('2d');
    //     var myChart = new Chart(ctx, {
    //         type: 'bar',
    //         data: {
    //             labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
    //             datasets: [{
    //                 label: '# of Votes',
    //                 data: [12, 19, 3, 5, 2, 3],
    //                 backgroundColor: [
    //                     'rgba(255, 99, 132, 0.2)',
    //                     'rgba(54, 162, 235, 0.2)',
    //                     'rgba(255, 206, 86, 0.2)',
    //                     'rgba(75, 192, 192, 0.2)',
    //                     'rgba(153, 102, 255, 0.2)',
    //                     'rgba(255, 159, 64, 0.2)'
    //                 ],
    //                 borderColor: [
    //                     'rgba(255, 99, 132, 1)',
    //                     'rgba(54, 162, 235, 1)',
    //                     'rgba(255, 206, 86, 1)',
    //                     'rgba(75, 192, 192, 1)',
    //                     'rgba(153, 102, 255, 1)',
    //                     'rgba(255, 159, 64, 1)'
    //                 ],
    //                 borderWidth: 1
    //             }]
    //         },
    //         options: {
    //             scales: {
    //                 yAxes: [{
    //                     ticks: {
    //                         beginAtZero: true
    //                     }
    //                 }]
    //             }
    //         }
    //     });
    // }

    // async _updateObject(event, formData) {
    //     console.log(formData.exampleInput);
    // }
}

Hooks.on("ready", () => {
    loadStatApp();
});

function loadStatApp() {
    window.StatApp = StatApp;

    new StatApp('example').render(true);
}